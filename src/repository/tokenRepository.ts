import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Injectable } from '@nestjs/common';
import { TokenModel } from 'src/entity/persistence';

@EntityRepository(TokenModel)
@Injectable()
export class TokenRepository extends BaseRepository<TokenModel> { }
