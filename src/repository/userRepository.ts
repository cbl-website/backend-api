import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Injectable } from '@nestjs/common';
import { UserModel } from 'src/entity/persistence';

@EntityRepository(UserModel)
@Injectable()
export class UserRepository extends BaseRepository<UserModel> { }
