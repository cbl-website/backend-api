import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Role } from 'src/entity';
import { Injectable } from '@nestjs/common';

@EntityRepository(Role)
@Injectable()
export class RoleRepository extends BaseRepository<Role> { }
