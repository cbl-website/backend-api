import { CommandBus, QueryBus } from '@nestjs/cqrs';

export abstract class BaseService {
  protected readonly commandBus: CommandBus;
  protected readonly queryBus: QueryBus;

  public constructor(commandBus: CommandBus, queryBus: QueryBus) {
    this.commandBus = commandBus;
    this.queryBus = queryBus;
  }
}
