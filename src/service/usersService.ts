import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { COMMON } from 'src/constants';
import { BaseService } from './baseService';
import { User } from 'src/entity';
import { CreateUserCommand, LoginUserCommand } from 'src/command/users';
import { Inject, Injectable } from '@nestjs/common';
import { GetUserByUsernameOrEmailQuery, GetUserByTokenValueQuery } from 'src/query/users';
import { CreateUserDto, LoginUserDto } from 'src/dto/users';

@Injectable()
export class UsersService extends BaseService {
  public constructor(
    @Inject(COMMON.CommandBus) commandBus: CommandBus,
    @Inject(COMMON.QueryBus) queryBus: QueryBus,
  ) {
    super(commandBus, queryBus);
  }

  public async createUser(createUserDto: CreateUserDto): Promise<User> {
    return this.commandBus.execute(
      new CreateUserCommand(createUserDto),
    );
  }

  public async loginUser(loginUserDto: LoginUserDto): Promise<User> {
    return this.commandBus.execute(
      new LoginUserCommand(loginUserDto),
    );
  }

  public async getByTokenValue(value: string) {
    return this.queryBus.execute<GetUserByTokenValueQuery, User>(
      new GetUserByTokenValueQuery(value),
    );
  }

  public async getByUsernameOrEmail(usernameOrEmail: string) {
    return this.queryBus.execute<GetUserByUsernameOrEmailQuery, User>(
      new GetUserByUsernameOrEmailQuery(usernameOrEmail),
    );
  }
}
