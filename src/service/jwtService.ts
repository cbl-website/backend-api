import { exists } from 'src/util';
import { Inject } from '@nestjs/common';
import { CONFIG } from 'src/constants';
import { differenceInMilliseconds, isBefore } from 'date-fns';
import { InvalidArgumentException } from 'src/exception/common';
import { IJwtServiceConfig } from 'src/config/service';
import * as jwt from 'jsonwebtoken';

export class JwtService {
  private readonly config: IJwtServiceConfig;

  public constructor(@Inject(CONFIG.IJwtServiceConfig) config: IJwtServiceConfig) {
    this.config = config;
  }

  public generateToken(expirationDate?: Date) {
    if (exists(expirationDate) && isBefore(expirationDate, new Date())) {
      throw new InvalidArgumentException('Token expiration date has to be in the future.');
    }

    const miliseconds = exists(expirationDate) ? differenceInMilliseconds(expirationDate, new Date()) : 1000 * 60 * 60 * 24 * 30;
    return jwt.sign({}, this.config.privateKey, { expiresIn: miliseconds });
  }

  public verifyToken(token: string) {
    try {
      jwt.verify(token, this.config.privateKey);
      return true;
    } catch (exception) {
      return false;
    }
  }
}
