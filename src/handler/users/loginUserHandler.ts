import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { LoginUserCommand } from 'src/command/users';
import { User, Token } from 'src/entity';
import { DatabaseException, NotAuthenticatedException } from 'src/exception/common';
import { Injectable } from '@nestjs/common';
import { UserRepository } from 'src/repository';
import { JwtService } from 'src/service';
import { exists } from 'src/util';

@CommandHandler(LoginUserCommand)
@Injectable()
export class LoginUserHandler implements ICommandHandler<LoginUserCommand> {
  private readonly userRepository: UserRepository;
  private readonly jwtService: JwtService;
  private readonly publisher: EventPublisher;

  public constructor(userRepository: UserRepository, jwtService: JwtService) {
    this.userRepository = userRepository;
    this.jwtService = jwtService;
  }

  public async execute(command: LoginUserCommand) {
    let user: User | undefined;

    try {
      user = await this.userRepository.findOne({
        where: [
          { username: command.usernameOrEmail },
          { email: command.usernameOrEmail },
        ],
        relations: [
          'tokens',
        ],
      });
    } catch (exception) {
      throw new DatabaseException();
    }

    if (!exists(user)) {
      throw new NotAuthenticatedException('Invalid credentials.');
    }

    if (!user.active) {
      throw new NotAuthenticatedException('Account not activated.');
    }

    const isValidPassword = await user.verifyPassword(command.password);
    if (!isValidPassword) {
      throw new NotAuthenticatedException('Invalid credentials.');
    }

    const jwt = this.jwtService.generateToken();
    const token = new Token({
      value: jwt,
      active: true,
    });

    user.tokens.push(token);
    try {
      const savedUser = this.publisher.mergeObjectContext(
        await this.userRepository.save(user),
      );

      savedUser.publishLoginUser();
      savedUser.commit();
      return savedUser;
    } catch (exception) {
      throw new DatabaseException();
    }
  }
}
