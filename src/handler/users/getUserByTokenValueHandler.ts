import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetUserByTokenValueQuery } from 'src/query/users';
import { UserRepository } from 'src/repository';
import { Injectable } from '@nestjs/common';

@QueryHandler(GetUserByTokenValueQuery)
@Injectable()
export class GetUserByTokenValueHandler implements IQueryHandler<GetUserByTokenValueQuery> {
  private readonly userRepository: UserRepository;

  public constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }

  public async execute(query: GetUserByTokenValueQuery) {
    return this.userRepository.findOne({ where: { tokens: { value: query.value } }, relations: ['tokens'] });
  }
}
