import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { CreateUserCommand } from 'src/command/users';
import { User } from 'src/entity';
import { DatabaseException } from 'src/exception/common';
import { Injectable } from '@nestjs/common';
import { UserRepository } from 'src/repository';

@CommandHandler(CreateUserCommand)
@Injectable()
export class CreateUserHandler implements ICommandHandler<CreateUserCommand> {
  private readonly userRepository: UserRepository;
  private readonly publisher: EventPublisher;

  public constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }

  public async execute(command: CreateUserCommand) {
    const userModel = new User({
      username: command.username,
      password: command.password,
      email: command.email,
      active: false,
    });

    try {
      const user = this.publisher.mergeObjectContext(
        await this.userRepository.save(userModel),
      );

      user.publishCreateUser(command.roleId);
      user.commit();
      return user;
    } catch (exception) {
      throw new DatabaseException();
    }
  }
}
