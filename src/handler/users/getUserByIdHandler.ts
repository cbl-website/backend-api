import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetUserByIdQuery } from 'src/query/users';
import { UserRepository } from 'src/repository';
import { Injectable } from '@nestjs/common';

@QueryHandler(GetUserByIdQuery)
@Injectable()
export class GetUserByIdHandler implements IQueryHandler<GetUserByIdQuery> {
  private readonly userRepository: UserRepository;

  public constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }

  public async execute(query: GetUserByIdQuery) {
    return this.userRepository.findOne({ id: query.id });
  }
}
