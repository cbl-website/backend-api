import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Role, Permission } from 'src/entity';
import { DatabaseException } from 'src/exception/common';
import { Injectable } from '@nestjs/common';
import { RoleRepository } from 'src/repository';
import { AddUserRoleCommand } from 'src/command/roles';
import { exists } from 'fs';

@CommandHandler(AddUserRoleCommand)
@Injectable()
export class AddUserRoleHandler implements ICommandHandler<AddUserRoleCommand> {
  private readonly roleRepository: RoleRepository;

  public constructor(roleRepository: RoleRepository) {
    this.roleRepository = roleRepository;
  }

  public async execute(command: AddUserRoleCommand) {
    let role: Role | undefined;

    try {
      role = await this.roleRepository.findOne({ id: command.roleId });
    } catch (exception) {
      throw new DatabaseException();
    }

    try {
      await this.roleRepository.save(role);
    } catch (exception) {
      throw new DatabaseException();
    }
  }
}
