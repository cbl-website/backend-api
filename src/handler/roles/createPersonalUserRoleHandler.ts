import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Role, Permission } from 'src/entity';
import { DatabaseException } from 'src/exception/common';
import { Injectable } from '@nestjs/common';
import { RoleRepository } from 'src/repository';
import { CreatePersonalUserRoleCommand } from 'src/command/roles';

@CommandHandler(CreatePersonalUserRoleCommand)
@Injectable()
export class CreatePersonalUserRoleHandler implements ICommandHandler<CreatePersonalUserRoleCommand> {
  private readonly roleRepository: RoleRepository;

  public constructor(roleRepository: RoleRepository) {
    this.roleRepository = roleRepository;
  }

  public async execute(command: CreatePersonalUserRoleCommand) {
    const roleModel = new Role({
      name: 'Self',
      userIds: [command.userId],
      permissions: [
        new Permission({ value: `users:get` }),
        new Permission({ value: `users:${command.userId}:get,update,delete` }),
        new Permission({ value: `roles:get` }),
      ],
    });

    try {
      await this.roleRepository.save(roleModel);
    } catch (exception) {
      throw new DatabaseException();
    }
  }
}
