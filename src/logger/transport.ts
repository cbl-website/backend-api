import { LogLevel, LogEntry } from './logger';
import { exists } from 'src/util';
import { ConfigurationException } from 'src/exception/common';

export interface TransportOptions {
  stream: NodeJS.WritableStream;
  minLevel: LogLevel;
  maxLevel?: LogLevel;
  transform: (entry: LogEntry) => any;
}

export class Transport {
  private readonly options: TransportOptions;

  public constructor(options: TransportOptions) {
    if (exists(options.maxLevel) && options.maxLevel < options.minLevel) {
      throw new ConfigurationException('Max logger level has to be of a higher or equal level to min logger level.');
    }

    this.options = options;
  }

  public write(entry: LogEntry) {
    if (exists(this.options.maxLevel) && this.options.maxLevel < entry.level) {
      return;
    }

    if (this.options.minLevel > entry.level) {
      return;
    }

    this.options.stream.write(this.options.transform(entry));
  }
}
