import { CONFIG } from 'src/constants';
import { ILoggerConfig } from 'src/config/common';
import { Transport } from './transport';
import { exists } from 'src/util';
import { baseTransform } from './transform';
import { Injectable, Inject } from '@nestjs/common';

export enum LogLevel {
  Debug = 10,
  Info = 20,
  Warn = 30,
  Error = 40,
}

export interface LogOptions {
  application: string;
  transports: Array<Transport>;
}

export interface LogEntry {
  data: object | Error;
  timestamp: Date;
  message: string;
  level: LogLevel;
  error?: Error;
}

export interface ILogger {
  debug(message: string, data?: object, error?: Error): void;
  info(message: string, data?: object, error?: Error): void;
  warn(message: string, data?: object, error?: Error): void;
  error(message: string, data?: object, error?: Error): void;
}

@Injectable()
export class Logger implements ILogger {
  private readonly options: LogOptions;

  public constructor(@Inject(CONFIG.ILoggerConfig) config: ILoggerConfig) {
    this.options = {
      application: config.application,
      transports: [
          new Transport({
            stream: process.stdout,
            minLevel: LogLevel.Debug,
            maxLevel: LogLevel.Warn,
            transform: baseTransform,
          }),
          new Transport({
            stream: process.stderr,
            minLevel: LogLevel.Error,
            transform: baseTransform,
          }),
        ],
    };
  }

  private createLogEntry(message: string, level: LogLevel, data?: object, error?: Error): LogEntry {
    return {
      data: { ...data, application: this.options.application},
      timestamp: new Date(),
      message,
      level,
      error,
    };
  }

  private logMessage(message: string, level: LogLevel, data?: object, error?: Error) {
    const logEntry = this.createLogEntry(message, level, data, error);

    this.options.transports.forEach((transport) => {
      transport.write(logEntry);
    });
  }

  public debug(message: string, data?: object, error?: Error) {
    this.logMessage(message, LogLevel.Debug, data, error);
  }

  public info(message: string, data?: object, error?: Error) {
    this.logMessage(message, LogLevel.Info, data, error);
  }

  public warn(message: string, data?: object, error?: Error) {
    this.logMessage(message, LogLevel.Warn, data, error);
  }

  public error(message: string, data?: object, error?: Error) {
    this.logMessage(message, LogLevel.Error, data, error);
  }
}
