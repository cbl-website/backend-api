import { LogEntry } from './logger';
import { exists } from 'src/util';
import { pickBy } from 'lodash';
import { SeqEvent, SeqLogLevel } from 'seq-logging';

export function baseTransform(entry: LogEntry) {
  const { data, ...props } = entry;

  const filteredProps = pickBy(props, (value) => exists(value));
  return JSON.stringify({
    ...data,
    ...filteredProps,
  });
}

export interface LogLevelMapping {
  [level: number]: SeqLogLevel,
}

export function seqTransform(entry: LogEntry): SeqEvent {
  const logLevel: LogLevelMapping = {
    10: 'Debug',
    20: 'Information',
    30: 'Warning',
    40: 'Error',
  }

  const timestamp = exists(entry.timestamp) ? entry.timestamp : new Date();
  const level = logLevel[entry.level];
  const messageTemplate = entry.message;

  if (exists(entry.error)) {
    const { message: errorMessage, stack: errorStack, ...errorProps } = entry.error;
    return {
      timestamp,
      level,
      messageTemplate,
      properties: { ...entry.data, ...errorProps, errorMessage },
      exception: errorStack,
    }
  } else {
    return {
      timestamp,
      level,
      messageTemplate,
      properties: { ...entry.data },
    }
  }
}
