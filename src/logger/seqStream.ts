import { Stream } from 'stream';
import { SeqLogger, SeqLoggerConfig, SeqEvent } from 'seq-logging';
import { exists } from 'src/util';

export class SeqStream extends Stream.Writable {
  private readonly logger: SeqLogger;

  public constructor(config: SeqLoggerConfig) {
    super({ objectMode: true });

    const onError = (e: Error) => {
      this.destroy();
      if (exists(config)) {
        config.onError(e);
      }
    };

    this.logger = new SeqLogger({ ...config, onError });
  }

  public _write(event: SeqEvent, encoding: string, callback?: ((error: Error | null | undefined) => void)): void {
    try {
      this.logger.emit(event);
    } catch (error) {
      console.error(error);
    }

    if (exists(callback)) {
      callback(undefined);
    }
  }

  public _final(callback: ((error: Error | null | undefined) => void)) {
    this.logger
      .close()
      .then(() => callback(undefined))
      .catch((error) => callback(error));
  }
}
