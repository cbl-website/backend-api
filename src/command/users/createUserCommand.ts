import { ICommand } from '@nestjs/cqrs';
import { CreateUserDto } from 'src/dto/users';

export class CreateUserCommand implements ICommand {
  public readonly username: string;
  public readonly password: string;
  public readonly email: string;
  public readonly roleId: string;

  public constructor(createUserDto: CreateUserDto) {
    this.username = createUserDto.username;
    this.password = createUserDto.password;
    this.email = createUserDto.email;
    this.roleId = createUserDto.roleId;
  }
}
