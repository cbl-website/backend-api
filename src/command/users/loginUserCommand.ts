import { ICommand } from '@nestjs/cqrs';
import { LoginUserDto } from 'src/dto/users';

export class LoginUserCommand implements ICommand {
  public readonly usernameOrEmail: string;
  public readonly password: string;

  public constructor(loginUserDto: LoginUserDto) {
    this.usernameOrEmail = loginUserDto.usernameOrEmail;
    this.password = loginUserDto.password;
  }
}
