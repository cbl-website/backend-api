import { ICommand } from '@nestjs/cqrs';

export class AddUserRoleCommand implements ICommand {
  public readonly userId: string;
  public readonly roleId: string;

  public constructor(userId: string, roleId: string) {
    this.userId = userId;
    this.roleId = roleId;
  }
}
