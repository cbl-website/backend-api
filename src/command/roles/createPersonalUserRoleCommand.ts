import { ICommand } from '@nestjs/cqrs';

export class CreatePersonalUserRoleCommand implements ICommand {
  public readonly userId: string;

  public constructor(userId: string) {
    this.userId = userId;
  }
}
