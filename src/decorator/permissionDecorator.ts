import { SetMetadata } from '@nestjs/common';

export const UsePermission = (permission: string) => SetMetadata('permission', permission);
