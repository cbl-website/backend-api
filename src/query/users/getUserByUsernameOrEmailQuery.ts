import { IQuery } from '@nestjs/cqrs';

export class GetUserByUsernameOrEmailQuery implements IQuery {
  public readonly usernameOrEmail: string;

  public constructor(usernameOrEmail: string) {
    this.usernameOrEmail = usernameOrEmail;
  }
}
