import { IQuery } from '@nestjs/cqrs';

export class GetUserByTokenValueQuery implements IQuery {
  public readonly value: string;

  public constructor(value: string) {
    this.value = value;
  }
}
