import { IQuery } from '@nestjs/cqrs';

export class GetUserByIdQuery implements IQuery {
  public readonly id: string;

  public constructor(id: string) {
    this.id = id;
  }
}
