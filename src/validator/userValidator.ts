import { Injectable } from '@nestjs/common';
import { ValidationResult } from './validationResult';
import { UserRepository } from 'src/repository';
import { exists } from 'src/util';

@Injectable()
export class UserValidator {
  private readonly userRepository: UserRepository;

  public constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }

  public async validateUsername(username: string) {
    if (username.length < 3) {
      return new ValidationResult(false, 'Username has to have a minimum length of 3.');
    }

    const fetchedUser = await this.userRepository.findOne({ username });
    if (exists(fetchedUser)) {
      return new ValidationResult(false, 'Username taken.');
    }

    return new ValidationResult(true);
  }

  public async validateEmail(email: string) {
    const regex = /[^@]+@[^\.]+\..+/;
    if (!regex.test(email)) {
      return new ValidationResult(false, 'Not a valid email.');
    }

    const fetchedUser = await this.userRepository.findOne({ email });
    if (exists(fetchedUser)) {
      return new ValidationResult(false, 'Email taken.');
    }

    return new ValidationResult(true);
  }
}
