import { exists } from 'src/util';
import { InvalidArgumentException } from 'src/exception/common';

export class ValidationResult {
  public readonly isSuccess: boolean;
  public readonly errorMessage?: string;

  public constructor(success: boolean, errorMessage?: string) {
    if (success && exists(errorMessage)) {
      throw new InvalidArgumentException('Cannot set error message on successful result.');
    }

    if (!success && !exists(errorMessage)) {
      throw new InvalidArgumentException('A failing result has to have an error message.');
    }

    this.isSuccess = success;
    this.errorMessage = errorMessage;
  }
}
