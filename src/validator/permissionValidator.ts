import { Injectable } from '@nestjs/common';
import { ValidationResult } from './validationResult';

@Injectable()
export class PermissionValidator {
  public validateValue(value: string) {
    const regex = /^((\w|-)+|\*)((:|,)((\w|-)+|\*))*$/g;
    if (!regex.test(value)) {
      return new ValidationResult(false, 'Invalid permission string.');
    }

    return new ValidationResult(true);
  }
}
