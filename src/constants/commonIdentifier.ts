export const COMMON = {
  CookieName: 'ChillButLitUser',
  CommandBus: Symbol.for('CommandBus'),
  QueryBus: Symbol.for('QueryBus'),
};
