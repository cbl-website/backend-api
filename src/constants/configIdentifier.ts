export const CONFIG = {
  ILoggerConfig: Symbol.for('ILoggerConfig'),
  IAppConfig: Symbol.for('IAppServiceConfig'),
  IDatabaseConfig: Symbol.for('IDatabaseConfig'),
  IJwtServiceConfig: Symbol.for('IJwtServiceConfig'),
}
