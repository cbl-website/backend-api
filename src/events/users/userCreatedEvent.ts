import { IEvent } from '@nestjs/cqrs';

export class UserCreatedEvent implements IEvent {
  public readonly userId: string;
  public readonly roleId: string;

  public constructor(userId: string, roleId: string) {
    this.userId = userId;
    this.roleId = roleId;
  }
}
