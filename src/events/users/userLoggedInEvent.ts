import { IEvent } from '@nestjs/cqrs';

export class UserLoggedInEvent implements IEvent {
  public readonly userId: string;

  public constructor(userId: string) {
    this.userId = userId;
  }
}
