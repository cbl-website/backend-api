import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TJwtServiceConfig>;
export interface IJwtServiceConfig extends T { }

export const TJwtServiceConfig = t.type({
  privateKey: t.string,
});
