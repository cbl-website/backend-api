import { Type as YamlType } from 'js-yaml';
import { ConfigurationException } from 'src/exception/common';

export const envType = new YamlType('!env', {
  kind: 'scalar',
  resolve(name: string) {
    if (Reflect.has(process.env, name)) {
      return true;
    } else {
      throw new ConfigurationException(`environment variable not found: ${name}`);
    }
  },
  construct(name: string) {
    return Reflect.get(process.env, name);
  },
});
