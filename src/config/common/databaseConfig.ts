import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TDatabaseConfig>;
export interface IDatabaseConfig extends T { }

export const TDatabaseConfig = t.type({
  host: t.string,
  port: t.string,
  username: t.string,
  password: t.string,
  database: t.string,
});
