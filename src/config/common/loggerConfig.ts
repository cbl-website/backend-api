import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TLoggerConfig>;
export interface ILoggerConfig extends T { }

export const TLoggerConfig = t.type({
  application: t.string,
});
