import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TAppConfig>;
export interface IAppConfig extends T { }

export const TAppConfig = t.type({
  serviceName: t.string,
  corsOrigin: t.string,
  port: t.number,
});
