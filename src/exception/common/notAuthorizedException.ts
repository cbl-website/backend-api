import { HttpException, HttpStatus } from '@nestjs/common';
import { exists } from 'src/util';

export class NotAuthorizedException extends HttpException {
  constructor(message?: string) {
    const msg = exists(message) ? message : 'User not authorized.';
    super(msg, HttpStatus.FORBIDDEN);
  }
}
