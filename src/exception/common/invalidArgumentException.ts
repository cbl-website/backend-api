import { HttpException, HttpStatus } from '@nestjs/common';
import { exists } from 'src/util';

export class InvalidArgumentException extends HttpException {
  constructor(message?: string) {
    const msg = exists(message) ? message : 'Invalid argument provided.';
    super(msg, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
