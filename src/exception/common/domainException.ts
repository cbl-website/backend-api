import { HttpException, HttpStatus } from '@nestjs/common';
import { exists } from 'src/util';

export class DomainException extends HttpException {
  constructor(message: string, status?: HttpStatus) {
    super(message, exists(status) ? status : HttpStatus.CONFLICT);
  }
}
