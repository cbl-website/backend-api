import { HttpException, HttpStatus } from '@nestjs/common';
import { exists } from 'src/util';

export class NotAuthenticatedException extends HttpException {
  constructor(message?: string) {
    const msg = exists(message) ? message : 'User not authenticated.';
    super(msg, HttpStatus.UNAUTHORIZED);
  }
}
