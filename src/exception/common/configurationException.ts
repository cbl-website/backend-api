import { HttpException, HttpStatus } from '@nestjs/common';
import { exists } from 'src/util';

export class ConfigurationException extends HttpException {
  constructor(message?: string) {
    const msg = exists(message) ? message : 'Invalid configuration.';
    super(msg, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
