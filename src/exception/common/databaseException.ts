import { HttpException, HttpStatus } from '@nestjs/common';

export class DatabaseException extends HttpException {
  constructor() {
    super('A database exception occured.', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
