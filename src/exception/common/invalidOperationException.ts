import { HttpException, HttpStatus } from '@nestjs/common';
import { exists } from 'src/util';

export class InvalidOperationException extends HttpException {
  constructor(message?: string) {
    const msg = exists(message) ? message : 'Invalid operation.';
    super(msg, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
