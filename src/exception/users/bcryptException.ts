import { HttpException, HttpStatus } from '@nestjs/common';

export class BcryptException extends HttpException {
  constructor() {
    super('Bcrypt failed to hash password.', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
