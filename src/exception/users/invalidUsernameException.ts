import { HttpException, HttpStatus } from '@nestjs/common';
import { exists } from 'src/util';

export class InvalidUsernameException extends HttpException {
  constructor(message?: string) {
    super(exists(message) ? message : 'Invalid username provided.', HttpStatus.CONFLICT);
  }
}
