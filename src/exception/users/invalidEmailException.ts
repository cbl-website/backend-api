import { HttpException, HttpStatus } from '@nestjs/common';
import { exists } from 'src/util';

export class InvalidEmailException extends HttpException {
  constructor(message?: string) {
    super(exists(message) ? message : 'Invalid email provided.', HttpStatus.CONFLICT);
  }
}
