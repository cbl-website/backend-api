import { HttpException, HttpStatus } from '@nestjs/common';

export class InvalidPermissionStringException extends HttpException {
  constructor() {
    super('Invalid permission string provided.', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
