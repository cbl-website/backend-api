import { Request } from 'express';
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { COMMON } from 'src/constants';
import { exists, mustExist } from 'src/util';
import { UsersService, JwtService } from 'src/service';
import { Reflector } from '@nestjs/core';
import * as shiroTrie from 'shiro-trie';
import { NotAuthenticatedException } from 'src/exception/common';

@Injectable()
export class AuthGuard implements CanActivate {
  private readonly usersService: UsersService;
  private readonly jwtService: JwtService;
  private readonly reflector: Reflector;

  public constructor(
    jwtService: JwtService,
    usersService: UsersService,
    reflector: Reflector,
  ) {
    this.jwtService = jwtService;
    this.usersService = usersService;
    this.reflector = reflector;
  }

  public async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest<Request>();

    const jwt = request.cookies[COMMON.CookieName];
    if (!exists(jwt)) {
      throw new NotAuthenticatedException('Missing auth token.');
    }

    const isValid = this.jwtService.verifyToken(jwt);
    if (!isValid) {
      throw new NotAuthenticatedException('Invalid auth token.');
    }

    const user = await this.usersService.getByTokenValue(jwt);
    const userToken = mustExist(user.tokens.find((token) => token.value === jwt));
    if (!userToken.active) {
      throw new NotAuthenticatedException('Inactive auth token.');
    }

    const permission = this.reflector.get<string>('permission', context.getHandler());
    if (exists(permission)) {
      const trie = shiroTrie.newTrie();
    }

    return true;
  }
}
