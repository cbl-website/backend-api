import { CqrsModule } from '@nestjs/cqrs';
import { Module } from '@nestjs/common';
import { UsersService } from 'src/service';
import { CreateUserHandler } from 'src/handler/users/createUserHandler';
import { GetUserByIdHandler } from 'src/handler/users/getUserByIdHandler';
import { GetUserByTokenValueHandler } from 'src/handler/users/getUserByTokenValueHandler';
import { UsersController } from 'src/controller';
import { UserRepository } from 'src/repository';

const commandHandlers = [
  CreateUserHandler,
];

const queryHandlers = [
  GetUserByIdHandler,
  GetUserByTokenValueHandler,
];

@Module({
  imports: [CqrsModule],
  controllers: [UsersController],
  providers: [
    UsersService,
    UserRepository,
    ...commandHandlers,
    ...queryHandlers,
  ],
})
export class UsersModule { }
