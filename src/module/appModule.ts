import { Module, NestModule, MiddlewareConsumer, Inject } from '@nestjs/common';
import { ConfigModule } from './configModule';
import { CONFIG } from 'src/constants';
import { IAppConfig } from 'src/config/common';
import { LoggerModule } from './loggerModule';
import { Logger } from 'src/logger';
import { DatabaseModule } from './databaseModule';
import { UsersModule } from './usersModule';
import * as cors from 'cors';
import * as cookieParser from 'cookie-parser';

@Module({
  imports: [
    ConfigModule,
    LoggerModule,
    DatabaseModule,
    UsersModule,
  ],
})
export class AppModule implements NestModule {
  private readonly config: IAppConfig;
  private readonly logger: Logger;

  public constructor(
    @Inject(CONFIG.IAppConfig) config: IAppConfig,
    logger: Logger,
  ) {
    this.config = config;
    this.logger = logger;
  }

  public configure(consumer: MiddlewareConsumer) {
    this.logger.info('Configuring middleware...');

    consumer.apply(
      cors({ origin: this.config.corsOrigin, credentials: true }),
      cookieParser(),
    );

    this.logger.info('Middleware configured successfully.');
  }
}
