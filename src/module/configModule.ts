import { Module, Global } from '@nestjs/common';
import { loadConfig } from 'src/config/core';
import { TAppConfig, IAppConfig, IDatabaseConfig, TDatabaseConfig} from 'src/config/common';
import { CONFIG } from 'src/constants';
import { ILoggerConfig, TLoggerConfig } from 'src/config/common';
import { IJwtServiceConfig, TJwtServiceConfig } from 'src/config/service';

@Global()
@Module({
  providers: [{
    provide: CONFIG.IAppConfig,
    useValue: loadConfig<IAppConfig>(TAppConfig, '/usr/src/app/config/appConfig.yml'),
  }, {
    provide: CONFIG.ILoggerConfig,
    useValue: loadConfig<ILoggerConfig>(TLoggerConfig, '/usr/src/app/config/loggerConfig.yml'),
  }, {
    provide: CONFIG.IDatabaseConfig,
    useValue: loadConfig<IDatabaseConfig>(TDatabaseConfig, '/usr/src/app/config/databaseConfig.yml'),
  }, {
    provide: CONFIG.IJwtServiceConfig,
    useValue: loadConfig<IJwtServiceConfig>(TJwtServiceConfig, '/usr/src/app/config/jwtServiceConfig.yml'),
  }],
  exports: [
    CONFIG.IAppConfig,
    CONFIG.ILoggerConfig,
    CONFIG.IDatabaseConfig,
    CONFIG.IJwtServiceConfig,
  ],
})
export class ConfigModule { }
