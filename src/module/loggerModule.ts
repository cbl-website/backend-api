import { Module, Global } from '@nestjs/common';
import { Logger } from 'src/logger';

@Global()
@Module({
  providers: [Logger],
  exports: [Logger],
})
export class LoggerModule { }
