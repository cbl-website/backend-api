import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CONFIG } from 'src/constants';
import { IDatabaseConfig } from 'src/config/common';
import { ConfigModule } from './configModule';
import { Permission, Role, User, Token } from 'src/entity';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [CONFIG.IDatabaseConfig],
      useFactory: async (config: IDatabaseConfig) => ({
        type: 'postgres' as 'postgres',
        host: config.host,
        port: parseInt(config.port, 10),
        username: config.username,
        password: config.password,
        database: config.database,
        entities: [
          Permission,
          Token,
          Role,
          User,
        ],
      }),
    }),
  ],
})
export class DatabaseModule { }
