import { Controller, Body, Post } from '@nestjs/common';
import { UsersService } from 'src/service';
import { CreateUserDto, LoginUserDto } from 'src/dto/users';
import { UsePermission } from 'src/decorator';

@Controller('users')
export class UsersController {
  private readonly usersService: UsersService;

  public constructor(usersService: UsersService) {
    this.usersService = usersService;
  }

  @Post()
  @UsePermission('user:create')
  public async createUser(@Body() createUserDto: CreateUserDto) {
    const user = await this.usersService.createUser(createUserDto);
    return {
      id: user.id,
      username: user.username,
      email: user.email,
    }
  }

  @Post()
  public async loginUser(@Body() loginUserDto: LoginUserDto) {
    const user = await this.usersService.loginUser(loginUserDto);
    return {
      id: user.id,
      username: user.username,
      email: user.email,
      roleIds: user.roles.map((role) => role.id),
    }
  }
}
