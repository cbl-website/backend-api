import 'reflect-metadata';
import { NestFactory } from '@nestjs/core';
import { AppModule } from 'src/module';
import { initializeTransactionalContext } from 'typeorm-transactional-cls-hooked';

async function bootstrap() {
  initializeTransactionalContext();

  const application = await NestFactory.create(AppModule);
  await application.listen(3000);
}

/* tslint:disable: no-floating-promises */
bootstrap();
