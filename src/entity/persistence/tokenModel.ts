import { Entity, PrimaryColumn, Column, ManyToOne } from 'typeorm';
import { UserModel } from './userModel';

@Entity({ name: 'Token' })
export class TokenModel {
  @PrimaryColumn('uuid')
  public id: string;

  @Column({ nullable: false })
  public value: string;

  @Column({ nullable: false })
  public active: boolean;

  @ManyToOne(() => UserModel, (user) => user.tokens)
  public user: UserModel;
}
