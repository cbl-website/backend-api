import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm';
import { TokenModel } from './tokenModel';

@Entity({ name: 'User' })
export class UserModel {
  @PrimaryColumn('uuid')
  public id: string;

  @Column({ nullable: false })
  public username: string;

  @Column({ nullable: false })
  public email: string;

  @Column()
  public password: string;

  @Column({ nullable: false })
  public active: boolean;

  @OneToMany(() => TokenModel, (token) => token.user)
  public tokens: Array<TokenModel>;
}
