import * as uuid from 'uuid';
import * as bcrypt from 'bcrypt';
import { exists, mustExist } from 'src/util';
import { Column, Entity, OneToMany, PrimaryColumn, ManyToMany } from 'typeorm';
import { Token } from './token';
import { AggregateRoot } from '@nestjs/cqrs';
import { BcryptException } from 'src/exception/users';
import { UserCreatedEvent } from 'src/events/users';
import { UserLoggedInEvent } from 'src/events/users/userLoggedInEvent';
import { UserValidator } from 'src/validator';
import { DomainException } from 'src/exception/common';
import { Role } from './role';

export interface UserProps {
  id?: string;
  username: string;
  email: string;
  active: boolean;
  tokens: Array<Token>;
}

@Entity({ name: 'User' })
export class User extends AggregateRoot {
  @PrimaryColumn({ type: 'uuid', name: 'id' })
  private _id: string;

  @Column({ nullable: false })
  public active: boolean;

  @Column({ nullable: false, name: 'username' })
  private _username: string;

  @Column({ nullable: false, name: 'email' })
  private _email: string;

  @Column({ nullable: false, name: 'password' })
  private _password: string;

  @OneToMany(() => Token, (token: Token) => token.user)
  public tokens: Array<Token>;

  @ManyToMany(() => Role, (role) => role.users)
  public readonly roles: Array<Role>;

  public get id() {
    return this._id;
  }

  public get username() {
    return this._username;
  }

  public get email() {
    return this._email;
  }

  public get password() {
    return this._password;
  }

  public async setUsername(username: string, validator: UserValidator) {
    const validationResult = await validator.validateUsername(username);
    if (!validationResult.isSuccess) {
      throw new DomainException(mustExist(validationResult.errorMessage));
    }

    this._username = username;
  }

  public async setEmail(email: string, validator: UserValidator) {
    const validationResult = await validator.validateEmail(email);
    if (!validationResult.isSuccess) {
      throw new DomainException(mustExist(validationResult.errorMessage));
    }

    this._email = email;
  }

  public async setPassword(plaintextPassword: string) {
    const hashed = await this.hashPassword(plaintextPassword);
    this._password = hashed;
  }

  public verifyPassword(plaintextPassword: string) {
    return bcrypt.compare(plaintextPassword, this._password);
  }

  private async hashPassword(plaintextPassword: string) {
    try {
      return await bcrypt.hash(plaintextPassword, 10);
    } catch {
      throw new BcryptException();
    }
  }

  public publishCreateUser(roleId: string) {
    this.apply(new UserCreatedEvent(this.id, roleId));
  }

  public publishLoginUser() {
    this.apply(new UserLoggedInEvent(this.id));
  }

  public static async create(props: UserProps, validator: UserValidator) {
    const user = new User();
    user._id = exists(props.id) ? props.id : uuid.v4();
    user.active = props.active;
    user.tokens = props.tokens;
    await user.setUsername(props.username, validator);
    await user.setEmail(props.email, validator);

    return user;
  }
}
