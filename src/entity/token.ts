import * as uuid from 'uuid';
import { Column, PrimaryColumn, Entity, ManyToOne } from 'typeorm';
import { exists } from 'src/util';
import { User } from './user';

export interface TokenProps {
  id?: string;
  value: string;
  active: boolean;
}

@Entity({ name: 'Token' })
export class Token {
  @PrimaryColumn({ type: 'uuid', name: 'id' })
  private _id: string;

  @Column({ nullable: false })
  public value: string;

  @Column({ nullable: false })
  public active: boolean;

  public get id() {
    return this._id;
  }

  @ManyToOne(() => User, (user: User) => user.tokens)
  public readonly user: User;

  public static create(props: TokenProps) {
    const token = new Token();
    token._id = exists(props.id) ? props.id : uuid.v4();
    token.value = props.value;
    token.active = props.active;

    return token;
  }
}
