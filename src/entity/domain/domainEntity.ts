import { AggregateRoot as BaseAggregateRoot } from '@nestjs/cqrs';
import { exists } from 'src/util';
import * as uuid from 'uuid';

export abstract class AggregateRoot<T> extends BaseAggregateRoot {
  protected readonly id: string;

  protected constructor(id?: string) {
    super();

    this.id = exists(id) ? id : uuid.v4();
  }

  public abstract toPersistence(): T;
}
