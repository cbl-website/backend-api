import { AggregateRoot } from '../domainEntity';
import { UserRepository } from 'src/repository';
import { UserModel } from 'src/entity/persistence/userModel';
import { DomainException } from 'src/exception/common';
import { BcryptException } from 'src/exception/users';
import { exists } from 'src/util';
import * as bcrypt from 'bcrypt';

export interface CreateUserProps {
  username: string;
  email: string;
}

export class User extends AggregateRoot<UserModel> {
  private _username: string;
  private _email: string;
  private _password: string;
  private _active: boolean;

  public get username() {
    return this._username;
  }

  public get email() {
    return this._email;
  }

  public get password() {
    return this._password;
  }

  public get active() {
    return this._active;
  }

  public async setUsername(username: string, repository: UserRepository) {
    if (username.length < 3) {
      throw new DomainException('Username cannot be shorter than 3 characters.');
    }

    const fetchedUser = await repository.findOne({ username });
    if (exists(fetchedUser)) {
      throw new DomainException('Username taken.');
    }

    this._username = username;
  }

  public async setEmail(email: string, repository: UserRepository) {
    const regex = /[^@]+@[^\.]+\..+/;
    if (!regex.test(email)) {
      throw new DomainException('Invalid email provided.');
    }

    const fetchedUser = await repository.findOne({ email });
    if (exists(fetchedUser)) {
      throw new DomainException('Email taken.');
    }

    this._email = email;
  }

  public verifyPassword(plaintextPassword: string) {
    return bcrypt.compare(plaintextPassword, this._password);
  }

  private async hashPassword(plaintextPassword: string) {
    try {
      return await bcrypt.hash(plaintextPassword, 10);
    } catch {
      throw new BcryptException();
    }
  }

  public async setPassword(plaintextPassword: string) {
    const hashed = await this.hashPassword(plaintextPassword);
    this._password = hashed;
  }

  public static fromPersistance(model: UserModel) {
    const user = new User(model.id);
    user._username = model.username;
    user._email = model.email;
    user._password = model.password;
    user._active = model.active;

    return user;
  }

  public toPersistence() {
    const model = new UserModel();
    model.id = this.id;
    model.username = this.username;
    model.email = this.email;
    model.password = this.password;
    model.active = this.active;

    return model;
  }

  public static async create(props: CreateUserProps, repository: UserRepository) {
    const user = new User();
    user._active = false;
    await user.setUsername(props.username, repository);
    await user.setEmail(props.email, repository);

    return user;
  }
}
