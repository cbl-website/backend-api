import * as uuid from 'uuid';
import { Column, PrimaryColumn, Entity, OneToMany, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { exists } from 'src/util';
import { Permission } from './permission';
import { User } from './user';
import { AggregateRoot } from '@nestjs/cqrs';

export interface RoleProps {
  id?: string;
  name: string;
  permissions: Array<Permission>;
  userIds: Array<string>;
}

@Entity({ name: 'Role' })
export class Role extends AggregateRoot {
  @PrimaryColumn({ type: 'uuid', name: 'id' })
  private _id: string;

  @Column({ nullable: false })
  public name: string;

  @OneToMany(() => Permission, (permission) => permission.role)
  public permissions: Array<Permission>;

  @RelationId((role: Role) => role.users)
  public userIds: Array<string>;

  @ManyToMany(() => User, (user) => user.roles)
  @JoinTable()
  public readonly users: Array<User>;

  public get id() {
    return this._id;
  }

  public static create(props: RoleProps) {
    const role = new Role();
    role._id = exists(props.id) ? props.id : uuid.v4();
    role.name = props.name;
    role.permissions = props.permissions;
    role.userIds = props.userIds;
  }
}
