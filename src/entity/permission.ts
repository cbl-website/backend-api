import * as uuid from 'uuid';
import { Column, PrimaryColumn, Entity, ManyToOne } from 'typeorm';
import { exists, mustExist } from 'src/util';
import { Role } from './role';
import { PermissionValidator } from 'src/validator';
import { DomainException } from 'src/exception/common';

export interface PermissionProps {
  id?: string;
  value: string;
}

@Entity({ name: 'Permission' })
export class Permission {
  @PrimaryColumn({ type: 'uuid', name: 'id' })
  private _id: string;

  @Column({ nullable: false, name: 'value' })
  private _value: string;

  @ManyToOne(() => Role, (role) => role.permissions)
  public readonly role: Role;

  public get id() {
    return this._id;
  }

  public get value() {
    return this._value;
  }

  public setValue(value: string, validator: PermissionValidator) {
    const validationResult = validator.validateValue(value);
    if (!validationResult.isSuccess) {
      throw new DomainException(mustExist(validationResult.errorMessage));
    }

    this._value = value;
  }

  public create(props: PermissionProps, validator: PermissionValidator) {
    const permission = new Permission();
    permission._id = exists(props.id) ? props.id : uuid.v4();
    permission.setValue(props.value, validator);

    return permission;
  }
}
