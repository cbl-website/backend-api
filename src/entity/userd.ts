import * as uuid from 'uuid';
import * as bcrypt from 'bcrypt';
import { exists, mustExist } from 'src/util';
import { Column, Entity, OneToMany, PrimaryColumn, ManyToMany } from 'typeorm';
import { Token } from './token';
import { AggregateRoot } from '@nestjs/cqrs';
import { BcryptException } from 'src/exception/users';
import { UserCreatedEvent } from 'src/events/users';
import { UserLoggedInEvent } from 'src/events/users/userLoggedInEvent';
import { UserValidator } from 'src/validator';
import { DomainException } from 'src/exception/common';
import { Role } from './role';

export interface CreateUserProps {
  username: string;
  email: string;
}

export class User extends AggregateRoot {
  private _id: string;
  private _username: string;
  private _email: string;
  private _password: string;
  private _active: boolean;

  public get id() {
    return this._id;
  }

  public get username() {
    return this._username;
  }

  public get email() {
    return this._email;
  }

  public get password() {
    return this._password;
  }

  public get active() {
    return this._active;
  }

  public async setUsername(username: string, validator: UserValidator) {
    const validationResult = await validator.validateUsername(username);
    if (!validationResult.isSuccess) {
      throw new DomainException(mustExist(validationResult.errorMessage));
    }

    this._username = username;
  }

  public async setEmail(email: string, validator: UserValidator) {
    const validationResult = await validator.validateEmail(email);
    if (!validationResult.isSuccess) {
      throw new DomainException(mustExist(validationResult.errorMessage));
    }

    this._email = email;
  }

  public verifyPassword(plaintextPassword: string) {
    return bcrypt.compare(plaintextPassword, this._password);
  }

  private async hashPassword(plaintextPassword: string) {
    try {
      return await bcrypt.hash(plaintextPassword, 10);
    } catch {
      throw new BcryptException();
    }
  }

  public async setPassword(plaintextPassword: string) {
    const hashed = await this.hashPassword(plaintextPassword);
    this._password = hashed;
  }

  public static async create(props: CreateUserProps, validator: UserValidator) {
    const user = new User();
    user._id = uuid.v4();
    user._active = false;
    await user.setUsername(props.username, validator);
    await user.setEmail(props.email, validator);

    return user;
  }
}
