import { Injectable } from '@nestjs/common';
import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { UserCreatedEvent } from 'src/events/users';
import { CreateUserRoleCommand, AddUserRoleCommand } from 'src/command/roles';

@Injectable()
export class UserSagas {
  @Saga()
  public userCreated(events: Observable<any>): Observable<ICommand> {
    return events.pipe(
      ofType(UserCreatedEvent),
      map((event) => {
        return [
          new CreateUserRoleCommand(event.userId),
          new AddUserRoleCommand(event.userId, event.roleId),
        ]
      }),
      flatMap((c) => c),
    );
  }
}
